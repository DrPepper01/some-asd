import psycopg2

from plane.db.models.user import User
from plane.db.models import WorkspaceMember, Workspace

from dotenv import load_dotenv
import os

# Load environment variables from .env file
load_dotenv('/plane/apiserver/.env')


# def get_connection():
#     return psycopg2.connect(
#         dbname="plane",
#         user="plane",
#         password="plane",
#         host="192.168.2.107",
#         port="5442"
#     )

def get_connection():
    return psycopg2.connect(
        dbname=os.getenv('CRM_POSTGRES_DB'),
        user=os.getenv('CRM_POSTGRES_USER'),
        password=os.getenv('CRM_POSTGRES_PASSWORD'),
        host=os.getenv('CRM_POSTGRES_HOST'),
        port=os.getenv('CRM_POSTGRES_PORT')
    )


def connect(user_id: int):
    conn = get_connection()
    user_info = {'last_name': None, 'first_name': None}
    with conn.cursor() as cursor:
        sql = "SELECT last_name, first_name FROM postgres_main_schema.employee_v WHERE id= %s"
        cursor.execute(sql, (user_id,))
        user_data = cursor.fetchone()
        if user_data:
            user_info['last_name'], user_info['first_name'] = user_data
    conn.close()

    return user_info


def give_permissions( user_id: int, user: User, workspace_id=None):
    conn = get_connection()
    with conn.cursor() as cursor:
        sql = ("select user_id ,permission_code "
               "from postgres_main_schema.user_permission_v "
               "where user_id= %s ")
        cursor.execute(sql, (user_id,))
        print(user_id, "HEre the user_ID  $$$$$$$$$$$$$$$$$$$$$$")
        user_data = cursor.fetchall()

    conn.close()

    role_mapping = {'pm_owner': 20, 'pm_admin': 15, 'pm_member': 10, 'pm_guest': 5}
    role = next((role_mapping[user_role] for _, user_role in user_data if user_role in role_mapping), 10)

    workspace = Workspace.objects.get(id=workspace_id)

    workspace_member = WorkspaceMember.objects.filter(workspace=workspace, member=user).first()
    if workspace_member:
        workspace_member.role = role
        workspace_member.save()
    else:
        WorkspaceMember.objects.create(workspace=workspace, member=user, role=role)
    # print(user_data)
    return role


def take_role( user_id: int):
    conn = get_connection()
    with conn.cursor() as cursor:
        sql = ("select user_id ,permission_code "
               "from postgres_main_schema.user_permission_v "
               "where user_id= %s ")
        cursor.execute(sql, (user_id,))
        user_data = cursor.fetchall()

    conn.close()

    role_mapping = {'pm_owner': 20, 'pm_admin': 15, 'pm_member': 10, 'pm_guest': 5}
    role = next((role_mapping[user_role] for _, user_role in user_data if user_role in role_mapping), 10)
    return role


