from rest_framework import serializers


class ProjectAnalyticsSerializer(serializers.Serializer):
    total_issues_in_project = serializers.IntegerField()
    status_counts_in_project = serializers.DictField()
    priority_counts_in_project = serializers.DictField()
    issue_with_assignees = serializers.IntegerField()
    overude_issues = serializers.IntegerField()
    ended_issues = serializers.IntegerField()