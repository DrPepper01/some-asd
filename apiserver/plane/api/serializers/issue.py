from lxml import html


# Django imports
from django.utils import timezone

#  Third party imports
from rest_framework import serializers

# Module imports
from plane.db.models import (
    User,
    Issue,
    State,
    IssueAssignee,
    Label,
    IssueLabel,
    IssueLink,
    IssueComment,
    IssueAttachment,
    IssueActivity,
    ProjectMember,
)
from setuptools.config._validate_pyproject import ValidationError

from . import ProjectLiteSerializer
from .base import BaseSerializer
from .cycle import CycleSerializer, CycleLiteSerializer
from .module import ModuleSerializer, ModuleLiteSerializer
from .user import UserLiteSerializer
from .user import UserSerializer
from .state import StateLiteSerializer
from ...app.serializers import ProjectMemberSerializer
from ...db.models import Project
from rest_framework.serializers import SerializerMethodField


class IssueSerializer(BaseSerializer):
    # all_assignees = UserSerializer(many=True)
    # assignees = UserSerializer(many=True, write_only=True)
    attachments = serializers.SerializerMethodField()

    assignees = serializers.ListField(
        child=serializers.IntegerField(), write_only=True
    )


    # labels = serializers.ListField(
    #     child=serializers.PrimaryKeyRelatedField(
    #         queryset=Label.objects.values_list("id", flat=True)
    #     ),
    #     write_only=True,
    #     required=False,
    # )

    class Meta:
        model = Issue
        read_only_fields = [
            "id",
            "workspace",
            "project",
            "created_by",
            "updated_by",
            "created_at",
            "updated_at",
        ]
        exclude = [
            "description",
            "description_stripped",
        ]

    def validate(self, data):
        if (
            data.get("start_date", None) is not None
            and data.get("target_date", None) is not None
            and data.get("start_date", None) > data.get("target_date", None)
        ):
            raise serializers.ValidationError(
                "Start date cannot exceed target date"
            )

        try:
            if data.get("description_html", None) is not None:
                parsed = html.fromstring(data["description_html"])
                parsed_str = html.tostring(parsed, encoding="unicode")
                data["description_html"] = parsed_str

        except Exception as e:
            raise serializers.ValidationError(f"Invalid HTML: {str(e)}")

        # Validate assignees are from project

        # Validate labels are from project
        if data.get("labels", []):
            data["labels"] = Label.objects.filter(
                project_id=self.context.get("project_id"),
                id__in=data["labels"],
            ).values_list("id", flat=True)

        # Check parent issue is from workspace as it can be cross workspace
        if (
            data.get("parent")
            and not Issue.objects.filter(
                workspace_id=self.context.get("workspace_id"),
                pk=data.get("parent").id,
            ).exists()
        ):
            raise serializers.ValidationError(
                "Parent is not valid issue_id please pass a valid issue_id"
            )
        print('end validate')
        return data

    def create(self, validated_data):
        assignees = validated_data.pop("assignees", [])
        labels = validated_data.pop("labels", None)

        project_id = self.context["project_id"]
        workspace_id = self.context["workspace_id"]
        default_assignee_id = self.context["default_assignee_id"]

        # Create the issue without assigning any users yet
        issue = Issue.objects.create(**validated_data, project_id=self.context['project_id'])

        created_by_id = issue.created_by_id
        updated_by_id = issue.updated_by_id

        worker = None
        if assignees:
            for assignee_id in assignees:
                # Проверяем, существует ли пользователь с данным ID
                worker = User.objects.filter(username=assignee_id)
                if worker.exists():
                    IssueAssignee.objects.create(
                        assignee=worker.first(),
                        issue=issue,
                        project_id=project_id,
                        workspace_id=workspace_id,
                        created_by_id=created_by_id,
                        updated_by_id=updated_by_id,
                    )

        # issue.assignees.set(assignees)
        # Issue Audit Users

            # Then assign it to default assignee
        if default_assignee_id is not None:
            IssueAssignee.objects.create(
                assignee_id=default_assignee_id,
                issue=issue,
                project_id=project_id,
                workspace_id=workspace_id,
                created_by_id=created_by_id,
                updated_by_id=updated_by_id,
            )
        if labels is not None and len(labels):
            IssueLabel.objects.bulk_create(
                [
                    IssueLabel(
                        label_id=label_id,
                        issue=issue,
                        project_id=project_id,
                        workspace_id=workspace_id,
                        created_by_id=created_by_id,
                        updated_by_id=updated_by_id,
                    )
                    for label_id in labels
                ],
                batch_size=10,
            )
        return issue

    def update(self, instance, validated_data):
        assignees = validated_data.pop("assignees", None)
        labels = validated_data.pop("labels", None)

        # Related models
        project_id = instance.project_id
        workspace_id = instance.workspace_id
        created_by_id = instance.created_by_id
        updated_by_id = instance.updated_by_id

        if assignees is not None:
            IssueAssignee.objects.filter(issue=instance).delete()
            IssueAssignee.objects.bulk_create(
                [
                    IssueAssignee(
                        assignee_id=assignee_id,
                        issue=instance,
                        project_id=project_id,
                        workspace_id=workspace_id,
                        created_by_id=created_by_id,
                        updated_by_id=updated_by_id,
                    )
                    for assignee_id in assignees
                ],
                batch_size=10,
            )

        if labels is not None:
            IssueLabel.objects.filter(issue=instance).delete()
            IssueLabel.objects.bulk_create(
                [
                    IssueLabel(
                        label_id=label_id,
                        issue=instance,
                        project_id=project_id,
                        workspace_id=workspace_id,
                        created_by_id=created_by_id,
                        updated_by_id=updated_by_id,
                    )
                    for label_id in labels
                ],
                batch_size=10,
            )

        # Time updation occues even when other related models are updated
        instance.updated_at = timezone.now()

        for attr, value in validated_data.items():
            setattr(instance, attr, value)
        instance.save()

        return super().update(instance, validated_data)

    def to_representation(self, instance):
        # Start with the base representation
        data = super().to_representation(instance)

        # Serialize 'created_by' and 'updated_by' fields
        data['created_by'] = UserSerializer(instance.created_by, context=self.context).data
        data['updated_by'] = UserSerializer(instance.updated_by, context=self.context).data

        # Serialize 'attachments' field with correct context
        attachments = instance.issue_attachment.all()
        data['attachments'] = IssueAttachmentSerializer(attachments, many=True, context=self.context).data

        # Optionally serialize 'project' field
        project_serializer = ProjectLiteSerializer(instance.project, context=self.context)
        data['project'] = project_serializer.data

        # Optionally serialize 'assignees' field
        if "assignees" in self.fields:
            data["assignees"] = UserLiteSerializer(instance.assignees.all(), many=True, context=self.context).data

        # Optionally serialize 'labels' field
        if "labels" in self.fields:
            data["labels"] = LabelSerializer(instance.labels.all(), many=True, context=self.context).data

        return data

    def get_attachments(self, obj):
        attachments = obj.issue_attachment.all()
        return IssueAttachmentSerializer(attachments, many=True, context=self.context).data


class LabelSerializer(BaseSerializer):
    class Meta:
        model = Label
        fields = "__all__"
        read_only_fields = [
            "id",
            "workspace",
            "project",
            "created_by",
            "updated_by",
            "created_at",
            "updated_at",
        ]


class IssueLinkSerializer(BaseSerializer):
    class Meta:
        model = IssueLink
        fields = "__all__"
        read_only_fields = [
            "id",
            "workspace",
            "project",
            "issue",
            "created_by",
            "updated_by",
            "created_at",
            "updated_at",
        ]

    # Validation if url already exists
    def create(self, validated_data):
        if IssueLink.objects.filter(
            url=validated_data.get("url"),
            issue_id=validated_data.get("issue_id"),
        ).exists():
            raise serializers.ValidationError(
                {"error": "URL already exists for this Issue"}
            )
        return IssueLink.objects.create(**validated_data)


class IssueAttachmentSerializer(BaseSerializer):
    # asset_url = SerializerMethodField('get_asset_url')

    class Meta:
        model = IssueAttachment
        fields = '__all__'
        # read_only_fields = [
        #     "id",
        #     "workspace",
        #     "project",
        #     "issue",
        #     "created_by",
        #     "updated_by",
        #     "created_at",
        #     "updated_at",
        # ]

    # def get_asset_url(self, obj):
    #     request = self.context.get('request')
    #     if request:
    #         return request.build_absolute_uri(obj.asset.url)
    #     return None

    def to_representation(self, instance):
        data = super().to_representation(instance)
        created_by_serializer = UserSerializer(instance.created_by)
        data['created_by'] = created_by_serializer.data
        # data['asset_url'] = self.get_asset_url(instance)  # Ensure this line is here
        return data


class IssueCommentSerializer(BaseSerializer):
    is_member = serializers.BooleanField(read_only=True)

    class Meta:
        model = IssueComment
        read_only_fields = [
            "id",
            "workspace",
            "project",
            "issue",
            "created_by",
            "updated_by",
            "created_at",
            "updated_at",
        ]
        exclude = [
            "comment_stripped",
            "comment_json",
        ]

    def to_representation(self, instance):
        data = super().to_representation(instance)
        created_by_serializer = UserSerializer(instance.created_by)
        data['created_by'] = created_by_serializer.data
        return data

    def validate(self, data):
        try:
            if data.get("comment_html", None) is not None:
                parsed = html.fromstring(data["comment_html"])
                parsed_str = html.tostring(parsed, encoding="unicode")
                data["comment_html"] = parsed_str

        except Exception as e:
            raise serializers.ValidationError(f"Invalid HTML: {str(e)}")
        return data


class IssueActivitySerializer(BaseSerializer):
    class Meta:
        model = IssueActivity
        exclude = [
            "created_by",
            "updated_by",
        ]


class CycleIssueSerializer(BaseSerializer):
    cycle = CycleSerializer(read_only=True)

    class Meta:
        fields = [
            "cycle",
        ]


class ModuleIssueSerializer(BaseSerializer):
    module = ModuleSerializer(read_only=True)

    class Meta:
        fields = [
            "module",
        ]


class LabelLiteSerializer(BaseSerializer):
    class Meta:
        model = Label
        fields = [
            "id",
            "name",
            "color",
        ]


class IssueExpandSerializer(BaseSerializer):
    cycle = CycleLiteSerializer(source="issue_cycle.cycle", read_only=True)
    module = ModuleLiteSerializer(source="issue_module.module", read_only=True)
    labels = LabelLiteSerializer(read_only=True, many=True)
    assignees = UserLiteSerializer(read_only=True, many=True)
    state = StateLiteSerializer(read_only=True)

    class Meta:
        model = Issue
        fields = "__all__"
        read_only_fields = [
            "id",
            "workspace",
            "project",
            "created_by",
            "updated_by",
            "created_at",
            "updated_at",
        ]
