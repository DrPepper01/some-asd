# Module imports
from plane.db.models import Workspace
from .base import BaseSerializer

from rest_framework import serializers

from .base import BaseSerializer
from .user import UserLiteSerializer

from .user import UserSerializer

from plane.db.models import (
    User,
    Workspace,
    WorkspaceMember,
    Team,
    TeamMember,
    WorkspaceMemberInvite,
    WorkspaceTheme,
    WorkspaceUserProperties,
)

class WorkspaceLiteSerializer(BaseSerializer):
    """Lite serializer with only required fields"""

    class Meta:
        model = Workspace
        fields = [
            "name",
            "slug",
            "id",
        ]
        read_only_fields = fields


class WorkSpaceSerializer(serializers.ModelSerializer):
    owner = UserLiteSerializer(read_only=True)
    total_members = serializers.IntegerField(read_only=True)
    total_issues = serializers.IntegerField(read_only=True)
    children = serializers.SerializerMethodField()
    parent = serializers.PrimaryKeyRelatedField(queryset=Workspace.objects.all(), required=False)
    level = serializers.SerializerMethodField()
    members = serializers.SerializerMethodField()

    def get_members(self, obj):
        workspace_members = WorkspaceMember.objects.filter(workspace=obj).select_related('member')
        return UserSerializer([member.member for member in workspace_members], many=True).data

    def get_level(self, obj):
        if obj.parent is None:
            return 1
        elif obj.parent and obj.parent.parent is None:
            return 2
        else:
            return 3

    def validate_slug(self, value):
        forbidden_slugs = ["404", "api", "admin", "create-workspace"]
        if value in forbidden_slugs:
            raise serializers.ValidationError("This slug is not allowed.")
        return value

    def get_children(self, obj):
        children = obj.child_workspaces.all()
        return WorkSpaceSerializer(children, many=True, context=self.context).data


    class Meta:
        model = Workspace
        fields = "__all__"
        read_only_fields = [
            "id",
            "created_by",
            "updated_by",
            "created_at",
            "updated_at",
            "owner",
            "level",
        ]

