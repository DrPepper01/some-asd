# Third party imports
from rest_framework import serializers

# Module imports
from plane.db.models import (
    Project,
    ProjectIdentifier,
    WorkspaceMember,
    State,
    Estimate,
    ProjectMember
)
from setuptools.config._validate_pyproject import ValidationError

from .base import BaseSerializer
from .user import UserSerializer
from ...db.models import User



class ProjectSerializer(BaseSerializer):
    total_members = serializers.IntegerField(read_only=True)
    all_members = UserSerializer(many=True, read_only=True)
    total_cycles = serializers.IntegerField(read_only=True)
    total_modules = serializers.IntegerField(read_only=True)
    is_member = serializers.BooleanField(read_only=True)
    # sort_order = serializers.FloatField(read_only=True)
    member_role = serializers.IntegerField(read_only=True)
    is_deployed = serializers.BooleanField(read_only=True)
    # members = UserSerializer(many=True, write_only=True)
    members = serializers.ListField(
        child=serializers.IntegerField(), write_only=True
    )

    class Meta:
        model = Project
        fields = "__all__"
        read_only_fields = [
            "id",
            "emoji",
            "workspace",
            "created_at",
            "updated_at",
            "created_by",
            "updated_by",
        ]

    def validate(self, data):
        # Check project lead should be a member of the workspace
        if (
            data.get("project_lead", None) is not None
            and not WorkspaceMember.objects.filter(
                workspace_id=self.context["workspace_id"],
                member_id=data.get("project_lead"),
            ).exists()
        ):
            raise serializers.ValidationError(
                "Project lead should be a user in the workspace"
            )

        # Check default assignee should be a member of the workspace
        if (
            data.get("default_assignee", None) is not None
            and not WorkspaceMember.objects.filter(
                workspace_id=self.context["workspace_id"],
                member_id=data.get("default_assignee"),
            ).exists()
        ):
            raise serializers.ValidationError(
                "Default assignee should be a user in the workspace"
            )

        return data

    def create(self, validated_data):

        members_data = validated_data.pop("members", [])

        project = Project.objects.create(
            **validated_data, workspace_id=self.context["workspace_id"]
        )

        _ = ProjectIdentifier.objects.create(
            name=project.identifier,
            project=project,
            workspace_id=self.context["workspace_id"],
        )

        identifier = validated_data.get("identifier", "").strip()
        if identifier == "":
            raise serializers.ValidationError(
                detail="Project Identifier is required"
            )

        if ProjectIdentifier.objects.filter(
            name=identifier, workspace_id=self.context["workspace_id"]
        ).exists():
            raise serializers.ValidationError(
                detail="Project Identifier is taken"
            )

        for member_id in members_data:
            member = User.objects.get(username=member_id)
            ProjectMember.objects.create(project=project, member=member)

        return project

    def to_representation(self, instance):
        data = super().to_representation(instance)

        created_by_serializer = UserSerializer(instance.created_by)
        data['created_by'] = created_by_serializer.data

        updated_by_serializer = UserSerializer(instance.updated_by)
        data['updated_by'] = updated_by_serializer.data

        project_members = map(lambda project_member_instance: project_member_instance.member,
                              ProjectMember.objects.filter(project=instance))
        # member_data = User.objects.filter(username__in=project_members.values('member_id'))
        member_serializer = UserSerializer(project_members, many=True)
        data['all_members'] = member_serializer.data
        return data


class ProjectLiteSerializer(BaseSerializer):
    class Meta:
        model = Project
        fields = [
            "id",
            "identifier",
            "name",
            "cover_image",
            "icon_prop",
            "emoji",
            "description",
        ]
        read_only_fields = fields
