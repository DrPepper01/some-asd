# Django imports

from rest_framework import authentication
from rest_framework.exceptions import AuthenticationFailed

from plane.db.models.user import User
from plane.db.models import WorkspaceMember, Workspace

from plane.api.utils import get_connection, give_permissions, connect

import jwt

import logging


class APIKeyAuthentication(authentication.BaseAuthentication):
    """
    Authentication with an API Key
    """

    www_authenticate_realm = "api"
    media_type = "application/json"
    auth_header_name = "Authorization"
    secret_key = "this_is_secret_key_for_jwt"

    def get_api_token(self, request):
        auth_header = request.headers.get(self.auth_header_name, "")
        if not auth_header:
            return None
        auth_token = auth_header.split()
        if len(auth_token) != 2 or auth_token[0].lower() != 'bearer':
            return None
        return auth_token[1]

    def validate_api_token(self, token):
        payload = jwt.decode(token, self.secret_key, algorithms=['HS256'])
        user_id = payload.get('user_id')

        logging.info(f"Received user_id from token: {user_id}")  # adding logs

        try:
            # adding logs
            logging.info("Checking if the user exists in the database...")
            user = User.objects.get(username=user_id)
            logging.info("User exists in the database.")
            # if workspace_slug:
            #     give_permissions(user_id, user)  # imported method that checks n gives permissions for WorkspaceMembers

        except User.DoesNotExist:
            logging.info("User does not exist in the database.")
            # create new user
            logging.info("Creating a new user...")

            user_info = connect(user_id)

            first_name = user_info['first_name']
            last_name = user_info['last_name']
            user = User.objects.create(username=user_id, first_name=first_name, last_name=last_name)
            # take all info from 107 DB
            logging.info("New user created.")

            # if workspace_slug:
            #     give_permissions(user_id, user)  # imported method that checks n gives permissions for WorkspaceMembers

        except jwt.ExpiredSignatureError:
            raise AuthenticationFailed("Token has expired")
        except jwt.InvalidTokenError:
            raise AuthenticationFailed("Invalid token")

        return (user,token, user_id)

    def authenticate(self, request):
        token = self.get_api_token(request=request)
        if not token:
            return None

        user, token, user_id = self.validate_api_token(token)
        request.user_id = user_id
        return user, token
