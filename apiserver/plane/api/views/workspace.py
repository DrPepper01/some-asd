# Python imports
import jwt
from datetime import date, datetime
from dateutil.relativedelta import relativedelta

# Django imports
from django.db import IntegrityError
from django.conf import settings
from django.utils import timezone
from django.core.exceptions import ValidationError
from django.core.validators import validate_email
from django.db.models import (
    Prefetch,
    OuterRef,
    Func,
    F,
    Q,
    Count,
    Case,
    Value,
    CharField,
    When,
    Max,
    IntegerField,
)
from django.db.models.functions import ExtractWeek, Cast, ExtractDay
from django.db.models.fields import DateField

# Third party modules
from rest_framework import status
from rest_framework.response import Response
from rest_framework.permissions import AllowAny

# Module imports
from plane.app.serializers import (
    WorkSpaceMemberSerializer,
    TeamSerializer,
    WorkSpaceMemberInviteSerializer,
    UserLiteSerializer,
    ProjectMemberSerializer,
    WorkspaceThemeSerializer,
    IssueActivitySerializer,
    IssueSerializer,
    WorkspaceMemberAdminSerializer,
    WorkspaceMemberMeSerializer,
    ProjectMemberRoleSerializer,
    WorkspaceUserPropertiesSerializer,
    WorkspaceEstimateSerializer,
    StateSerializer,
    LabelSerializer,
)
from plane.app.views.base import BaseAPIView
from plane.app.views.base import BaseViewSet
from plane.api.serializers import WorkSpaceSerializer
from plane.db.models import (
    State,
    User,
    Workspace,
    WorkspaceMemberInvite,
    Team,
    ProjectMember,
    IssueActivity,
    Issue,
    WorkspaceTheme,
    IssueLink,
    IssueAttachment,
    IssueSubscriber,
    Project,
    Label,
    WorkspaceMember,
    CycleIssue,
    IssueReaction,
    WorkspaceUserProperties,
    Estimate,
    EstimatePoint,
)
from plane.app.permissions import (
    WorkSpaceBasePermission,
    WorkSpaceAdminPermission,
    WorkspaceEntityPermission,
    WorkspaceViewerPermission,
    WorkspaceUserPermission,
    ProjectLitePermission,
)
from plane.bgtasks.workspace_invitation_task import workspace_invitation
from plane.utils.issue_filters import issue_filters
from plane.bgtasks.event_tracking_task import workspace_invite_event

from plane.api.views import CustomAuthenticationView
from plane.api.middleware.api_authentication import APIKeyAuthentication
from django.db.models import Subquery
from django.shortcuts import get_object_or_404


class WorkSpaceViewSet(BaseViewSet):
    model = Workspace
    serializer_class = WorkSpaceSerializer
    authentication_classes = [APIKeyAuthentication]
    # permission_classes = [
    #     WorkSpaceBasePermission,
    # ]

    search_fields = [
        "name",
    ]
    filterset_fields = [
        "owner",
    ]

    lookup_field = "slug"

    def get_queryset(self):
        parent_id = self.request.query_params.get('parent', None)

        member_count = WorkspaceMember.objects.filter(
            workspace=OuterRef("id"),
            member__is_bot=False,
            is_active=True
        ).order_by().annotate(count=Func(F("id"), function="Count")).values("count")

        issue_count = Issue.issue_objects.filter(
            workspace=OuterRef("id")
        ).order_by().annotate(count=Func(F("id"), function="Count")).values("count")

        queryset = self.filter_queryset(
            super().get_queryset().select_related("owner")
        ).annotate(
            total_members=Subquery(member_count, output_field=IntegerField()),
            total_issues=Subquery(issue_count, output_field=IntegerField())
        ).select_related("owner").order_by("name")

        if parent_id is not None:
            queryset = queryset.filter(parent_id=parent_id)
        else:
            queryset = queryset.filter(parent__isnull=True)

        queryset = queryset.filter(
            workspace_member__member=self.request.user,
            workspace_member__is_active=True
        )

        return queryset

    def create(self, request, *args, **kwargs):
        try:
            parent_slug = kwargs.get('slug', None)

            parent_workspace = None
            slug = request.data.get("slug", False)
            name = request.data.get("name", False)


            if not name or not slug:
                return Response(
                    {"error": "Both name and slug are required"},
                    status=status.HTTP_400_BAD_REQUEST,
                )


            if len(name) > 80 or len(slug) > 48:
                return Response(
                    {
                        "error": "The maximum length for name is 80 and for slug is 48"
                    },
                    status=status.HTTP_400_BAD_REQUEST,
                )

            if parent_slug:
                parent_workspace = Workspace.objects.filter(slug=parent_slug).first()
                if not parent_workspace:
                    return Response(
                        {"error": "Parent workspace not found"},
                        status=status.HTTP_404_NOT_FOUND
                    )

            serializer = WorkSpaceSerializer(data=request.data, context={'request': request})

            if serializer.is_valid():
                workspace = serializer.save(owner=request.user, parent=parent_workspace)

            if serializer.is_valid():
                serializer.save(owner=request.user)
                # Create Workspace member
                _ = WorkspaceMember.objects.create(
                    workspace_id=serializer.data["id"],
                    member=request.user,
                    role=20,
                    company_role=request.data.get("company_role", ""),
                )
                return Response(
                    serializer.data, status=status.HTTP_201_CREATED
                )
            return Response(
                [serializer.errors[error][0] for error in serializer.errors],
                status=status.HTTP_400_BAD_REQUEST,
            )

        except IntegrityError as e:
            if "already exists" in str(e):
                return Response(
                    {"slug": "The workspace with the slug already exists"},
                    status=status.HTTP_410_GONE,
                )

    def create_grandson(self, request, parent_slug, child_slug):
        try:
            parent_workspace = Workspace.objects.filter(slug=child_slug, parent__slug=parent_slug).first()
            if not parent_workspace:
                return Response(
                    {"error": "Parent workspace not found"},
                    status=status.HTTP_404_NOT_FOUND
                )

            slug = request.data.get("slug")
            name = request.data.get("name")

            if not name or not slug:
                return Response(
                    {"error": "Both name and slug are required"},
                    status=status.HTTP_400_BAD_REQUEST,
                )
            if len(name) > 80 or len(slug) > 48:
                return Response(
                    {"error": "The maximum length for name is 80 and for slug is 48"},
                    status=status.HTTP_400_BAD_REQUEST,
                )

            serializer = WorkSpaceSerializer(data=request.data, context={'request': request})
            if serializer.is_valid():
                new_workspace = serializer.save(owner=request.user, parent=parent_workspace)
                WorkspaceMember.objects.create(
                    workspace=new_workspace,
                    member=request.user,
                    role=20,
                    company_role=request.data.get("company_role", "")
                )
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            else:
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        except IntegrityError as e:
            if "already exists" in str(e):
                return Response(
                    {"slug": "The workspace with the slug already exists"},
                    status=status.HTTP_410_GONE,
                )
        except Exception as e:
            return Response(
                {"error": str(e)},
                status=status.HTTP_500_INTERNAL_SERVER_ERROR
            )

    def create_child(self, request, slug):
        parent_workspace = Workspace.objects.filter(slug=slug).first()
        if not parent_workspace:
            return Response({'error': 'Родительское рабочее пространство не найдено'}, status=status.HTTP_404_NOT_FOUND)

        serializer = WorkSpaceSerializer(data=request.data)
        if serializer.is_valid():
            workspace = serializer.save(parent=parent_workspace, owner=request.user)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def retrieve(self, request, slug=None, child_slug=None):
        # if child_slug:
        #     workspace = Workspace.objects.filter(slug=child_slug, parent__slug=slug).first()
        # else:
        #     workspace = Workspace.objects.filter(slug=slug).first()
        #
        # if not workspace:
        #     return Response({"error": "Workspace not found"}, status=status.HTTP_404_NOT_FOUND)
        #
        # serializer = WorkSpaceSerializer(workspace)
        # return Response(serializer.data)
        workspace = Workspace.objects.filter(slug=slug).first()
        if workspace:
            serializer = WorkSpaceSerializer(workspace, context={'request': request})
            return Response(serializer.data)
        else:
            return Response({"error": "Workspace not found"}, status=status.HTTP_404_NOT_FOUND)

    def partial_update(self, request, slug=None):
        try:
            workspace = Workspace.objects.get(slug=slug)
            name = request.data.get('name')
            if name:
                if len(name) > 80:
                    return Response(
                        {"error": "The maximum length for name is 80"},
                        status=status.HTTP_400_BAD_REQUEST,
                    )
                workspace.name = name
                workspace.save()
                return Response(
                    self.get_serializer(workspace).data,
                    status=status.HTTP_200_OK
                )
            return Response(
                {"error": "Name field is required"},
                status=status.HTTP_400_BAD_REQUEST,
            )
        except Workspace.DoesNotExist:
            return Response(
                {"error": "Workspace not found"},
                status=status.HTTP_404_NOT_FOUND,
            )
        except Exception as e:
            return Response(
                {"error": str(e)},
                status=status.HTTP_500_INTERNAL_SERVER_ERROR,
            )

    def retrieve_child(self, request, parent_slug):
        try:
            parent_workspace = Workspace.objects.get(slug=parent_slug)
        except Workspace.DoesNotExist:
            return Response({"error": "Given slug is not valid"}, status=status.HTTP_404_NOT_FOUND)

        child_workspaces = parent_workspace.child_workspaces.all()
        if not child_workspaces:
            return Response({"response": "No child workspaces available"}, status=status.HTTP_200_OK)

        serializer = WorkSpaceSerializer(child_workspaces, many=True, context={'request': request})
        return Response(serializer.data)

    def retrieve_grandson(self, request, grandparent_slug, parent_slug, child_slug):
        try:
            grandparent = Workspace.objects.get(slug=grandparent_slug)
            parent = grandparent.child_workspaces.get(slug=parent_slug)
            child_workspace = parent.child_workspaces.filter(slug=child_slug)

            if child_workspace.exists():
                serializer = WorkSpaceSerializer(child_workspace, many=True, context={'request': request})
                return Response(serializer.data)
            else:
                return Response({"error": "Child workspace not found"}, status=status.HTTP_404_NOT_FOUND)
        except Workspace.DoesNotExist:
            return Response({"error": "Workspace not found"}, status=status.HTTP_404_NOT_FOUND)

    def retrieve_child_detail(self, request, parent_slug, child_slug):
        workspace = Workspace.objects.filter(slug=child_slug, parent__slug=parent_slug).all()
        if workspace:
            serializer = WorkSpaceSerializer(workspace, many=True, context={'request': request})
            return Response(serializer.data)
        else:
            return Response({"error": "Child workspace not found"}, status=status.HTTP_404_NOT_FOUND)

    def destroy(self, request, slug=None):
        try:
            workspace = Workspace.objects.get(slug=slug)
            workspace.delete()
            return Response(status=status.HTTP_204_NO_CONTENT)
        except Workspace.DoesNotExist:
            return Response(
                {"error": "Workspace not found"},
                status=status.HTTP_404_NOT_FOUND,
            )
        except Exception as e:
            return Response(
                {"error": str(e)},
                status=status.HTTP_500_INTERNAL_SERVER_ERROR,
            )

    def update_child(self, request, parent_slug, child_slug, grandparent_slug):
        try:
            parent_workspace = Workspace.objects.get(slug=parent_slug)

            workspace = parent_workspace.child_workspaces.get(slug=child_slug)
            serializer = WorkSpaceSerializer(workspace, data=request.data, partial=True, context={'request': request})
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_200_OK)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        except Workspace.DoesNotExist:
            return Response({"error": "Workspace not found"}, status=status.HTTP_404_NOT_FOUND)
        except Exception as e:
            return Response({"error": str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    def destroy_child(self, request, parent_slug, child_slug):
        try:
            parent_workspace = Workspace.objects.get(slug=parent_slug)
            workspace = parent_workspace.child_workspaces.get(slug=child_slug)
            workspace.delete()
            return Response(status=status.HTTP_204_NO_CONTENT)
        except Workspace.DoesNotExist:
            return Response({"error": "Workspace not found"}, status=status.HTTP_404_NOT_FOUND)
        except Exception as e:
            return Response({"error": str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    def destroy_grandchild(self, request, parent_slug, child_slug, grandparent_slug):
        try:
            grantparent_workspace = Workspace.objects.get(slug=grandparent_slug)
            parent_workspace = grantparent_workspace.child_workspaces.get(slug=parent_slug)
            child_workspace = parent_workspace.child_workspaces.get(slug=child_slug)
            child_workspace.delete()
            return Response(status=status.HTTP_204_NO_CONTENT)
        except Workspace.DoesNotExist:
            return Response({"error": "Workspace not found"}, status=status.HTTP_404_NOT_FOUND)
        except Exception as e:
            return Response({"error": str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)