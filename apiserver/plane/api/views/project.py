# Django imports
import json

from django.db import IntegrityError
from django.db.models import Exists, OuterRef, Q, F, Func, Subquery, Prefetch

from django.db.models import Value, IntegerField


# Third party imports
from rest_framework import status
from rest_framework.parsers import MultiPartParser, JSONParser, FormParser
from rest_framework.response import Response
from rest_framework.serializers import ValidationError

# Module imports
from plane.db.models import (
    Workspace,
    Project,
    ProjectFavorite,
    ProjectMember,
    ProjectDeployBoard,
    State,
    Cycle,
    Module,
    IssueProperty,
    Inbox,
    User
)
from plane.app.permissions import ProjectBasePermission
from plane.api.serializers import ProjectSerializer
from .base import BaseAPIView, WebhookMixin
from plane.api.middleware.api_authentication import APIKeyAuthentication

from django.db.models import Count
from django.db import transaction

class ProjectAPIEndpoint(WebhookMixin, BaseAPIView):
    """Project Endpoints to create, update, list, retrieve and delete endpoint"""

    serializer_class = ProjectSerializer
    model = Project
    webhook_event = "project"
    authentication_classes = [APIKeyAuthentication]
    # permission_classes = [
    #     ProjectBasePermission,
    # ]
    parser_classes = [MultiPartParser, FormParser, JSONParser]

    def get_queryset(self):
        grandparent_slug = self.kwargs.get("grandparent_slug")
        parent_slug = self.kwargs.get("parent_slug")
        child_slug = self.kwargs.get("child_slug")

        if grandparent_slug and parent_slug and child_slug:
            return Project.objects.filter(workspace__slug=child_slug, workspace__parent__slug=parent_slug,
                                          workspace__parent__parent__slug=grandparent_slug)

        elif grandparent_slug and parent_slug:
            return Project.objects.filter(workspace__slug=parent_slug, workspace__parent__slug=grandparent_slug)

        elif grandparent_slug:
            return Project.objects.filter(workspace__slug=grandparent_slug)

        return Project.objects.none()

    def get(self, request, *args, **kwargs):
        project_id = kwargs.get("project_id")
        if project_id:
            project = self.get_queryset().get(pk=project_id)
            serializer = ProjectSerializer(project, fields=self.fields, expand=self.expand)
            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            projects = self.get_queryset().order_by(request.GET.get("order_by", "-created_at"))
            return self.paginate(
                request=request,
                queryset=projects,
                on_results=lambda projects: ProjectSerializer(
                    projects,
                    many=True,
                    fields=self.fields,
                    expand=self.expand,
                ).data,
            )

    @transaction.atomic
    def post(self, request, grandparent_slug, parent_slug=None, child_slug=None):
        try:
            if child_slug:
                parent_workspace = Workspace.objects.get(slug=parent_slug, parent__slug=grandparent_slug)
                workspace = Workspace.objects.get(slug=child_slug, parent=parent_workspace)
            elif grandparent_slug and parent_slug:
                grandparent_workspace = Workspace.objects.get(slug=grandparent_slug)
                workspace = Workspace.objects.get(slug=parent_slug, parent=grandparent_workspace)
            elif grandparent_slug:
                workspace = Workspace.objects.get(slug=grandparent_slug)

            if not workspace:
                return Response({"error": "Specified workspace not found"}, status=status.HTTP_404_NOT_FOUND)

            serializer = ProjectSerializer(
                data={**request.data},
                context={"workspace_id": workspace.id}
            )
            if serializer.is_valid():

                validated_data = serializer.validated_data
                validated_data['members'] = request.data.get('members', [])

                project = serializer.save()

                # Add the user as Administrator to the project
                project_member = ProjectMember.objects.create(
                    project_id=project.id,
                    member=request.user,
                    role=20,
                )
                # Also create the issue property for the user
                _ = IssueProperty.objects.create(
                    project_id=project.id,
                    user=request.user,
                )

                if validated_data.get("project_lead") and validated_data["project_lead"] != request.user.id:
                    ProjectMember.objects.create(
                        project_id=project.id,
                        member_id=validated_data["project_lead"],
                        role=20,
                    )
                    # Also create the issue property for the user
                    IssueProperty.objects.create(
                        project_id=project.id,
                        user_id=validated_data["project_lead"],
                    )

                # Default states
                states = [
                    {
                        "name": "Backlog",
                        "color": "#A3A3A3",
                        "sequence": 15000,
                        "group": "backlog",
                        "default": True,
                    },
                    {
                        "name": "Todo",
                        "color": "#3A3A3A",
                        "sequence": 25000,
                        "group": "unstarted",
                    },
                    {
                        "name": "In Progress",
                        "color": "#F59E0B",
                        "sequence": 35000,
                        "group": "started",
                    },
                    {
                        "name": "Done",
                        "color": "#16A34A",
                        "sequence": 45000,
                        "group": "completed",
                    },
                    {
                        "name": "Cancelled",
                        "color": "#EF4444",
                        "sequence": 55000,
                        "group": "cancelled",
                    },
                ]

                State.objects.bulk_create(
                    [
                        State(
                            name=state["name"],
                            color=state["color"],
                            project=project,
                            sequence=state["sequence"],
                            workspace=project.workspace,
                            group=state["group"],
                            default=state.get("default", False),
                            created_by=request.user,
                        )
                        for state in states
                    ]
                )

                serializer = ProjectSerializer(project)
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        except IntegrityError as e:
            if "already exists" in str(e):
                return Response(
                    {"name": "The project name is already taken"},
                    status=status.HTTP_410_GONE,
                )
        except Workspace.DoesNotExist as e:
            return Response(
                {"error": "Workspace does not exist"},
                status=status.HTTP_404_NOT_FOUND,
            )
        except ValidationError as e:
            return Response(
                {"identifier": "The project identifier is already taken"},
                status=status.HTTP_410_GONE,
            )

    def patch(self, request, grandparent_slug, project_id=None,  parent_slug=None, child_slug=None):
        try:
            if child_slug:
                parent_workspace = Workspace.objects.get(slug=parent_slug, parent__slug=grandparent_slug)
                workspace = Workspace.objects.get(slug=child_slug, parent=parent_workspace)
            elif parent_slug:
                grandparent_workspace = Workspace.objects.get(slug=grandparent_slug)
                workspace = Workspace.objects.get(slug=parent_slug, parent=grandparent_workspace)
            else:
                workspace = Workspace.objects.get(slug=grandparent_slug)

            project = Project.objects.get(pk=project_id, workspace=workspace)

            members_data = request.data.get('members', [])
            existing_member_ids = [member.member.id for member in ProjectMember.objects.filter(project=project)]
            new_members_ids = [member_id for member_id in members_data if member_id not in existing_member_ids]

            members_to_delete = request.data.get('members_to_delete', [])

            for member_id in new_members_ids:
                try:
                    user = User.objects.get(id=member_id)
                    ProjectMember.objects.create(project=project, member=user)
                except User.DoesNotExist:
                    return Response(
                        {"error": "User does not exist"},
                        status=status.HTTP_404_NOT_FOUND)

            for members_id in members_to_delete:
                ProjectMember.objects.filter(member_id=members_id, project=project).delete()

            serializer = ProjectSerializer(
                project,
                data={**request.data},
                context={"workspace_id": workspace.id},
                partial=True,
            )

            if serializer.is_valid():
                serializer.save()
                if serializer.data["inbox_view"]:
                    Inbox.objects.get_or_create(
                        name=f"{project.name} Inbox",
                        project=project,
                        is_default=True,
                    )

                    # Create the triage state in Backlog group
                    State.objects.get_or_create(
                        name="Triage",
                        group="backlog",
                        description="Default state for managing all Inbox Issues",
                        project_id=project_id,
                        color="#ff7700",
                    )

                project = (
                    self.get_queryset()
                    .filter(pk=serializer.data["id"])
                    .first()
                )
                serializer = ProjectSerializer(project)
                return Response(serializer.data, status=status.HTTP_200_OK)
            return Response(
                serializer.errors, status=status.HTTP_400_BAD_REQUEST
            )
        except IntegrityError as e:
            if "already exists" in str(e):
                return Response(
                    {"name": "The project name is already taken"},
                    status=status.HTTP_410_GONE,
                )
        except (Project.DoesNotExist, Workspace.DoesNotExist):
            return Response(
                {"error": "Project does not exist"},
                status=status.HTTP_404_NOT_FOUND,
            )
        except ValidationError as e:
            return Response(
                {"identifier": "The project identifier is already taken"},
                status=status.HTTP_410_GONE,
            )

    def delete(self, request, grandparent_slug, project_id, parent_slug=None, child_slug=None):
        try:
            if child_slug:
                parent_workspace = Workspace.objects.get(slug=parent_slug, parent__slug=grandparent_slug)
                workspace = Workspace.objects.get(slug=child_slug, parent=parent_workspace)
            elif parent_slug:
                grandparent_workspace = Workspace.objects.get(slug=grandparent_slug)
                workspace = Workspace.objects.get(slug=parent_slug, parent=grandparent_workspace)
            else:
                workspace = Workspace.objects.get(slug=grandparent_slug)

            project = Project.objects.get(pk=project_id, workspace=workspace)
            project.delete()
            return Response(status=status.HTTP_204_NO_CONTENT)
        except Workspace.DoesNotExist:
            return Response({"error": "Workspace not found"}, status=status.HTTP_404_NOT_FOUND)
        except Project.DoesNotExist:
            return Response({"error": "Project not found"}, status=status.HTTP_404_NOT_FOUND)
        except Exception as e:
            return Response({"error": str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
