import psycopg2
from django.http import JsonResponse
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

from django.db import IntegrityError
from plane.db.models import WorkspaceMember, Workspace, User

from plane.api.middleware.api_authentication import APIKeyAuthentication

from plane.api.utils import get_connection, give_permissions


class UserController(APIView):
    authentication_classes = [APIKeyAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request, *args, **kwargs):
        first_name = request.GET.get('first_name')
        last_name = request.GET.get('last_name')
        first_name_pattern = f"%{first_name}%"
        last_name_pattern = f"%{last_name}%"
        conn = get_connection()

        with conn.cursor() as cursor:
            sql = "SELECT * FROM postgres_main_schema.employee_v WHERE (first_name ILIKE %s AND last_name ILIKE %s) OR (first_name ILIKE %s AND last_name ILIKE %s)"
            cursor.execute(sql, (first_name_pattern, last_name_pattern, last_name_pattern, first_name_pattern))

            employee_data = cursor.fetchall()
        conn.close()
        response_data = {
            'employees': employee_data
        }

        for emp in employee_data:
            user_id = emp[0]
            first_name = emp[1]
            last_name = emp[2]
            print(user_id, first_name, last_name)


            # response in JSON format


            user, created = User.objects.get_or_create(
                username=user_id,
                defaults={'first_name': first_name, 'last_name': last_name})

            if created:
                message = f"User - {first_name} {last_name} with id {user_id} is created."
                # status_code = status.HTTP_201_CREATED

                try:
                    # expecting permission_code data from DB for giving some role to
                    give_permissions(user_id, user)

                except IntegrityError as e:
                    message = f"Произошла ошибка IntegrityError: {str(e)}"

            else:
                message = f"Пользователь - {first_name} {last_name} с айди {user_id} найден."

        return Response(response_data)

    def post(self, request, *args, **kwargs):
        user_id = request.data.get('user_id')
        first_name = request.data.get('first_name')
        last_name = request.data.get('last_name')
        message = ""
        status_code = status.HTTP_200_OK

        # using get_or_create for searchin'n'creating
        user, created = User.objects.get_or_create(
            username=user_id,
            defaults={'first_name': first_name, 'last_name': last_name})

        if created:
            message = f"User - {first_name} {last_name} with id {user_id} is created."
            status_code = status.HTTP_201_CREATED

            try:
                # expecting permission_code data from DB for giving some role to
                give_permissions(user_id, user)

            except IntegrityError as e:
                message = f"Произошла ошибка IntegrityError: {str(e)}"
                status_code = status.HTTP_400_BAD_REQUEST
        else:
            message = f"Пользователь - {first_name} {last_name} с айди {user_id} найден."

        return Response({"message": message, "user_id": user_id}, status=status_code)
