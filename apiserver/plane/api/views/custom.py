from plane.api.middleware.api_authentication import APIKeyAuthentication


from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

from plane.db.models.user import User

from rest_framework import status

from plane.api.utils import take_role

from plane.db.models import Workspace, WorkspaceMember


class CustomAuthenticationView(APIView):
    authentication_classes = [APIKeyAuthentication]
    permission_classes = [IsAuthenticated]


    def get(self, request):
        user, token = request.user, request.auth
        user_id = getattr(request, 'user_id', None)
        print(user_id)
        if user_id is None:
            return Response({"error": "User ID not found"}, status=status.HTTP_404_NOT_FOUND)

        # workspace = Workspace.objects.first()
        # print(user_id, "USER (999999999999999999999999999999999")
        # member = WorkspaceMember.objects.get(workspace=workspace, member=user)`
        role = take_role(user_id)
        data = {
            "Version": "1.4",
            # "Role" : member.role
            "Role" : take_role(user_id)
        }
        return Response(data, status=status.HTTP_200_OK)
