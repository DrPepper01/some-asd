# Django imports
from django.db.models import Count, Sum, F, Q
from django.db.models.functions import ExtractMonth

# Third party imports
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.response import Response

# Module imports
from .base import BaseAPIView
from plane.db.models import Issue, AnalyticView, Workspace, State, Label

from plane.api.middleware.api_authentication import APIKeyAuthentication

from plane.db import models
from django.db.models import Value
from django.db import models

from django.shortcuts import get_object_or_404

from plane.db.models import Project

from datetime import datetime, timedelta
from datetime import date

from ..serializers import ProjectAnalyticsSerializer, IssueSerializer
from collections import defaultdict, OrderedDict

from django.db.models import Count
from django.utils.timezone import now

from ...app.views import BaseViewSet
from ...db.models import IssueAssignee


class CustomAnalyticEndpoint(BaseViewSet):
    queryset = Project.objects.all()
    authentication_classes = [APIKeyAuthentication]
    serializer_class = ProjectAnalyticsSerializer

    def get_object(self):
        return get_object_or_404(Project, id=self.kwargs.get('pk'))

    def get_dates_by_period(self, period):

        if period == 'today':
            start_date = datetime.combine(date.today(), datetime.min.time())
            end_date = datetime.combine(date.today(), datetime.max.time())
        elif period == 'week':
            today = date.today()
            start_date = today - timedelta(days=today.weekday())
            end_date = start_date + timedelta(days=6)
        elif period == 'month':
            today = date.today()
            start_date = today.replace(day=1)
            end_date = (start_date.replace(month=start_date.month % 12 + 1, day=1) - timedelta(days=1))
        elif period == 'year':
            today = date.today()
            start_date = today.replace(month=1, day=1)
            end_date = today.replace(month=12, day=31)
        else:
            start_date, end_date = None, None
        return start_date, end_date

    @action(detail=True, methods=['get'], url_path='issue-analytics')
    def issue_analytics(self, request, pk=None):
        project = self.get_object()
        today = datetime.now().date()

        # Получаем все задачи проекта
        total_issues_in_project = project.project_issue.count()

        # Задачи с назначенными исполнителями
        issue_with_assignees = project.project_issue.exclude(assignees=None).count()

        # Просроченные задачи
        overdue_issues_count = project.project_issue.filter(target_date__lt=today).count()

        # Завершенные задачи
        ended_issues = project.project_issue.filter(state='done').count()

        data = {
            'total_issues_in_project': total_issues_in_project,
            'issue_with_assignees': issue_with_assignees,
            'overdue_issues_count': overdue_issues_count,
            'ended_issues': ended_issues
        }

        all_issues_serializer = IssueSerializer(project.project_issue.all(),
                                                many=True,
                                                context={'request': request})

        # Сериализация назначенных задач
        assigned_issues_serializer = IssueSerializer(project.project_issue.exclude(assignees=None),
                                                     many=True,
                                                     context={'request': request})

        # Сериализация завершенных задач
        ended_issues_serializer = IssueSerializer(project.project_issue.filter(state='done'),
                                                  many=True,
                                                  context={'request': request})

        data.update({
            'all_issues_details': all_issues_serializer.data,
            'assigned_issues_details': assigned_issues_serializer.data,
            'ended_issues_details': ended_issues_serializer.data,
        })

        return Response(data)

    @action(detail=True, methods=['get'], url_path='state')
    def state_analytics(self, request, pk=None):
        project = self.get_object()
        period = request.query_params.get('period')
        start_date, end_date = self.get_dates_by_period(period)
        issues_in_project = project.project_issue.all()

        if start_date and end_date:
            issues_in_project = issues_in_project.filter(created_at__range=(start_date, end_date))

        status_counts = issues_in_project.values('state').annotate(total=Count('id')).order_by('state')

        data = OrderedDict([('status_counts', {item['state']: item['total'] for item in status_counts})])

            # Сериализация всех задач по статусам без агрегации
        issues_serialized_data = {}
        for status, _ in Issue.STATE_CHOICES:
            status_issues = issues_in_project.filter(state=status)
            serializer = IssueSerializer(status_issues, many=True, context={'request': request})
            issues_serialized_data[status] = serializer.data

            # Добавляем сериализованные данные после агрегированной информации
        data.update(issues_serialized_data)

        return Response(data)

    @action(detail=True, methods=['get'], url_path='priority')
    def priority_analytics(self, request, pk=None):
        project = self.get_object()
        period = request.query_params.get('period')
        start_date, end_date = self.get_dates_by_period(period)
        issues_in_project = project.project_issue.all()

        if start_date and end_date:
            issues_in_project = issues_in_project.filter(created_at__range=(start_date, end_date))

        priority_counts = issues_in_project.values('priority').annotate(total=Count('id')).order_by('priority')
        data = OrderedDict([('priority_counts', {item['priority']: item['total'] for item in priority_counts})])

        issues_serialized_data = {}
        for priority, _ in Issue.PRIORITY_CHOICES:
            priority_issues = issues_in_project.filter(priority=priority)
            serializer = IssueSerializer(priority_issues, many=True, context={'request': request})
            issues_serialized_data[priority] = serializer.data

        data.update(issues_serialized_data)

        return Response(data)