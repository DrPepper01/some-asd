from django.urls import path, include
from plane.api.views import WorkSpaceViewSet


urlpatterns = [
    path('workspaces/', WorkSpaceViewSet.as_view({
        'get': 'list',
        'post': 'create'
    }), name='workspace-list'),

    path('workspaces/<str:slug>/', WorkSpaceViewSet.as_view({
        'get': 'retrieve',
        'put': 'update',
        'patch': 'partial_update',
        'delete': 'destroy',
        'post': 'create_child'
    }), name='workspace-detail'),

    path('workspaces/<str:parent_slug>/childs/', WorkSpaceViewSet.as_view({
        'get': 'retrieve_child'
    }), name='workspace-child-detail'),

    path('workspaces/<str:parent_slug>/<str:child_slug>/', WorkSpaceViewSet.as_view({
        'get': 'retrieve_child_detail',
        'put': 'update_child',
        'patch': 'update_child',
        'delete': 'destroy_child',
        'post': 'create_grandson'
    }), name='workspace-child-detail'),

    path('workspaces/<str:parent_slug>/<str:child_slug>/childs/', WorkSpaceViewSet.as_view({
        'get': 'retrieve_grandson'
    }), name='workspace-grandson-detail'),

    path('workspaces/<str:grandparent_slug>/<str:parent_slug>/<str:child_slug>/',
         WorkSpaceViewSet.as_view({
            'get': 'retrieve_grandson',
            'patch': 'update_child',
            'delete': 'destroy_grandchild',
        }),
         name='workspace-child-detail'),


]
