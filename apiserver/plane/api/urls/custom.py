from django.urls import path

from plane.api.views  import CustomAuthenticationView

# from plane.apiserver.plane.api.views import CustomAuthenticationView

urlpatterns = [
    path(
        "custom_auth/", CustomAuthenticationView.as_view(), name='custom_auth'
    ),
]


