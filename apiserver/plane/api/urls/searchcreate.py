from django.urls import path

from plane.api.views  import UserController

# from plane.apiserver.plane.api.views import UserController

urlpatterns = [
    path(
        "search_create/", UserController.as_view(), name='search_create'
    ),
]