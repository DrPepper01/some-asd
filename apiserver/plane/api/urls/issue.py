from django.urls import path

from plane.api.views import (
    IssueAPIEndpoint,
    LabelAPIEndpoint,
    IssueLinkAPIEndpoint,
    IssueCommentAPIEndpoint,
    IssueActivityAPIEndpoint,
)

urlpatterns = [
    path('issues/my/', IssueAPIEndpoint.as_view(), name='my-issues'),
    # Issues for the top-level workspace
    path("workspaces/<str:grandparent_slug>/projects/<uuid:project_id>/issues/",
         IssueAPIEndpoint.as_view(),
         name="grandparent-issues-list"),
    path("workspaces/<str:grandparent_slug>/projects/<uuid:project_id>/issues/<uuid:issue_id>/",
         IssueAPIEndpoint.as_view(),
         name="grandparent-issue-detail"),

    # Issues for child workspaces
    path("workspaces/<str:grandparent_slug>/<str:parent_slug>/projects/<uuid:project_id>/issues/",
         IssueAPIEndpoint.as_view(),
         name="parent-issues-list"),
    path("workspaces/<str:grandparent_slug>/<str:parent_slug>/projects/<uuid:project_id>/issues/<uuid:issue_id>/",
         IssueAPIEndpoint.as_view(),
         name="parent-issue-detail"),

    # Issues for grandson workspaces
    path("workspaces/<str:grandparent_slug>/<str:parent_slug>/<str:child_slug>/projects/<uuid:project_id>/issues/",
         IssueAPIEndpoint.as_view(),
         name="child-issues-list"),
    path(
        "workspaces/<str:grandparent_slug>/<str:parent_slug>/<str:child_slug>/projects/<uuid:project_id>/issues/<uuid:issue_id>/",
        IssueAPIEndpoint.as_view(),
        name="child-issue-detail"),


    path(
        "workspaces/<str:slug>/projects/<uuid:project_id>/labels/",
        LabelAPIEndpoint.as_view(),
        name="label",
    ),
    path(
        "workspaces/<str:slug>/projects/<uuid:project_id>/labels/<uuid:pk>/",
        LabelAPIEndpoint.as_view(),
        name="label",
    ),
    path(
        "workspaces/<str:slug>/projects/<uuid:project_id>/issues/<uuid:issue_id>/links/",
        IssueLinkAPIEndpoint.as_view(),
        name="link",
    ),
    path(
        "workspaces/<str:slug>/projects/<uuid:project_id>/issues/<uuid:issue_id>/links/<uuid:pk>/",
        IssueLinkAPIEndpoint.as_view(),
        name="link",
    ),
    path(
        "workspaces/<str:slug>/projects/<uuid:project_id>/issues/<uuid:issue_id>/comments/",
        IssueCommentAPIEndpoint.as_view(),
        name="comment",
    ),
    path(
        "workspaces/<str:slug>/projects/<uuid:project_id>/issues/<uuid:issue_id>/comments/<uuid:pk>/",
        IssueCommentAPIEndpoint.as_view(),
        name="comment",
    ),
    path(
        "workspaces/<str:slug>/projects/<uuid:project_id>/issues/<uuid:issue_id>/activities/",
        IssueActivityAPIEndpoint.as_view(),
        name="activity",
    ),
    path(
        "workspaces/<str:slug>/projects/<uuid:project_id>/issues/<uuid:issue_id>/activities/<uuid:pk>/",
        IssueActivityAPIEndpoint.as_view(),
        name="activity",
    ),
]
