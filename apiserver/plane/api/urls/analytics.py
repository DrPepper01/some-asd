from django.urls import path


from plane.api.views.analytics import (
    CustomAnalyticEndpoint,
)
from rest_framework.routers import DefaultRouter

router = DefaultRouter()

# Регистрируем наш класс представления с роутером
router.register(r'workspaces/some-analytic', CustomAnalyticEndpoint, basename='plane-analytics')

# Получаем URL-адреса из роутера
urlpatterns = router.urls



# urlpatterns = [
#     path(
#         "workspaces/some-analytic/<uuid:pk>/",
#         CustomAnalyticEndpoint.as_view(),
#         name="plane-analyticscast2",
#     ),
#     path(
#         "workspaces/some-analytic/state/<uuid:pk>/",
#         CustomAnalyticEndpoint.as_view(),# эндпоинт для state
#         name="plane-analyticscast2",
#     )
#     ,    path(
#         "workspaces/some-analytic/priority/<uuid:pk>/",
#         CustomAnalyticEndpoint.as_view(),# эндпоинт для priority
#         name="plane-analyticscast2",
#     )
# ]