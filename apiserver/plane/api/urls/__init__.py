from .project import urlpatterns as project_patterns
from .state import urlpatterns as state_patterns
from .issue import urlpatterns as issue_patterns
from .cycle import urlpatterns as cycle_patterns
from .module import urlpatterns as module_patterns
from .inbox import urlpatterns as inbox_patterns
from .custom import urlpatterns as custom_patterns
from .searchcreate import urlpatterns as search_patterns
from .analytics import urlpatterns as analytic_patterns
from .workspace import urlpatterns as workspace_patterns


urlpatterns = [
    *project_patterns,
    *state_patterns,
    *issue_patterns,
    *cycle_patterns,
    *module_patterns,
    *inbox_patterns,
    *custom_patterns,
    *search_patterns,
    *analytic_patterns,
    *workspace_patterns,
]
