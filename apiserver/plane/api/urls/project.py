from django.urls import path

from plane.api.views import ProjectAPIEndpoint


urlpatterns = [

    path("workspaces/<str:grandparent_slug>/projects/",
         ProjectAPIEndpoint.as_view(),
         name="grandparent-projects-list"),
    path("workspaces/<str:grandparent_slug>/projects/<uuid:project_id>/",
         ProjectAPIEndpoint.as_view(),
         name="grandparent-projects-detail"),
    path("workspaces/<str:grandparent_slug>/<str:parent_slug>/projects/",
         ProjectAPIEndpoint.as_view(),
         name="parent-projects-list"),
    path("workspaces/<str:grandparent_slug>/<str:parent_slug>/projects/<uuid:project_id>/",
         ProjectAPIEndpoint.as_view(),
         name="parent-project-detail"),
    path("workspaces/<str:grandparent_slug>/<str:parent_slug>/<str:child_slug>/projects/",
         ProjectAPIEndpoint.as_view(),
         name="child-projects-list"),
    path("workspaces/<str:grandparent_slug>/<str:parent_slug>/<str:child_slug>/projects/<uuid:project_id>/",
         ProjectAPIEndpoint.as_view(),
         name="child-project-detail"),

]
