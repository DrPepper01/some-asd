from rest_framework import serializers


class ProjectAnalyticsSerializer(serializers.Serializer):
    total_issues_in_project = serializers.IntegerField()
    status_counts_in_other_projects = serializers.DictField(child=serializers.IntegerField())
