# Django imports
from django.db.models import Count, Sum, F, Q
from django.db.models.functions import ExtractMonth

# Third party imports
from rest_framework import status
from rest_framework.response import Response

# Module imports
from plane.app.views import BaseAPIView, BaseViewSet
from plane.app.permissions import WorkSpaceAdminPermission
from plane.db.models import Issue, AnalyticView, Workspace, State, Label
from plane.app.serializers import AnalyticViewSerializer
from plane.utils.analytics_plot import build_graph_plot
from plane.bgtasks.analytic_plot_export import analytic_export_task
from plane.utils.issue_filters import issue_filters

from plane.api.middleware.api_authentication import APIKeyAuthentication

from plane.db import models
from django.db.models import Value
from django.db import models

from django.shortcuts import get_object_or_404

from plane.db.models import Project

from plane.app.serializers.customanalytic import ProjectAnalyticsSerializer

from datetime import datetime, timedelta


class CustomAnalyticEndpoint(BaseAPIView):
    queryset = Project.objects.all()
    authentication_classes = [APIKeyAuthentication]
    serializer_class = ProjectAnalyticsSerializer

    def get_object(self):
        return get_object_or_404(Project, id=self.kwargs.get('pk'))

    def get(self, request, pk):
        project = Project.objects.get(id=pk)

        # Определяем временные границы в зависимости от выбранного периода
        today = datetime.now()
        start_date = datetime.now() - timedelta(days=1)
        end_date = today

        period = request.query_params.get('period')
        if period == 'today':
            start_date = today.replace(hour=0, minute=0, second=0, microsecond=0)
        elif period == 'week':
            start_date = today - timedelta(days=7)
        elif period == 'month':
            start_date = today - timedelta(days=30)


        # Фильтрация данных по времени
        if start_date:
            issues_related_to_project = project.project_issue.filter(created_at__gte=start_date,
                                                                     created_at__lte=end_date).count()
        else:
            issues_related_to_project = project.project_issue.count()

        status_counts_in_other_projects = {}
        for state_display, _ in Issue.STATE_CHOICES:
            count = Issue.objects.exclude(project=project).filter(state=state_display, created_at__gte=start_date,
                                                                  created_at__lte=end_date).count()
            status_counts_in_other_projects[state_display] = count

        # Создаем экземпляр сериализатора и передаем данные для сериализации
        serializer = ProjectAnalyticsSerializer(data={
            'total_issues_in_project': issues_related_to_project,
            'status_counts_in_other_projects': status_counts_in_other_projects
        })

        # Проверяем валидность данных и возвращаем сериализованные данные
        serializer.is_valid(raise_exception=True)
        return Response(serializer.data)