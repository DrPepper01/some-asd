import psycopg2


def fetch_data_from_db():
    try:
        conn = psycopg2.connect(
            dbname="afm",
            user="afm",
            password="afm007",
            host="192.168.2.107",
            port="5432"
        )
        cursor = conn.cursor()


        # Example query to fetch data
        cursor.execute("SELECT * FROM hr.user LIMIT 5")
        data = cursor.fetchall()
        print(data)
        return data

    except (Exception, psycopg2.DatabaseError) as error:
        print("Ошибка при работе с PostgreSQL", error)

    finally:
        if conn is not None:
            conn.close()
            print("Connection closed.")


fetch_data_from_db()
